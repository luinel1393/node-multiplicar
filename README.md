## Multiplicar Console App

Esta es una aplicación para generar archivos de tablas de multiplicar.

Ejercicio del curso de Node de Fernando Herrera en Udemy disponible en:
[https://www.udemy.com/course/node-de-cero-a-experto/](https://www.udemy.com/course/node-de-cero-a-experto/)

Ejecutar este comando

```
npm install
```
